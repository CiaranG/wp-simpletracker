=== WP Simple Tracker ===
Contributors: CiaranG,kamasheto 
Tags: issue tracker, tracker
Requires at least: 3.3
Tested up to: 4.0.1
Stable tag: 0.33

Adds a simple issue tracking system to your WordPress blog.

== Description ==

WP Simple Tracker is a functional (yet basic) issue tracking system to plug
right in your WordPress powered site! Operating an issue tracking system
has never been this easy!

Currently it can operate one tracker per blog. However the back-end was
designed to allow as many trackers as you might want. This will be targeted
in a later release.

This plugin is based on WP-IssueTracker by Mahmoud Sakr, which is no longer
maintained by the author. You can upgrade directly from that to this
(strongly recommended due to security issues in the orignial), by removing
it and installing this instead. Your existing issues will remain intact.

== Installation ==

Extract the zip file and just drop the contents in the wp-content/plugins/
directory of your WordPress installation and then activate the Plugin from
the Plugins page.

To insert the tracker, create a new post (preferably a page) and insert the
following content in the body:

<!--wp-simpletracker-1-->

And publish! You can now access your tracker via accessing the page you just
created.

Creating new issues and commenting on them can be done by any regular user.
Editing issues, changing status, etc, can only be done by a user with the
'simpletracker_manager' capability. Issues can only be assigned to a user
with the 'simpletracker_assignee' capability.

Two new Roles are created, Issue Manager and Issue Assignee, which can
normally be used to control these capabilities. However, for more complex
setups, you can install a Role Editing plugin (like "Members") and assign
the above capabilities directly to the roles of your choice.

For best appearance, use a single-column page template for the tracker
(no sidebar) and disable comments and pingbacks. but it will work either way.



== Changelog ==

=0.33=

*Issue is now linked in comment notification emails

=0.32=

*Better control of permissions, via two capabilities.

=0.31=

*Fixed a problem where some instances of the star icon were being fetched
over http even when using https.

=0.3=

*Javascript and CSS are now handled properly within the WordPress framework.
They will only be loaded on pages where they're actually needed.

=0.2=

*Fix some javascript vulnerabilities
*Change permission required to manage issues
*Move new issue for to its own page
*View new issue after reporting, instead of going back to list
*Auto-star an issue you change or comment on
*Fixed star toggling
*Various other improvements

=0.1=

First release of WP-IssueTracker, on which this is based.

== Notes ==



<?php
/*
Plugin Name: WP-SimpleTracker
Plugin URI: https://gitlab.com/CiaranG/wp-simpletracker
Description: Adds a simple issue tracking system to WordPress
Version: 0.33
Author: Ciaran Gultnieks, Mahmoud Sakr
Author URI: http://ciarang.com
License: GPL3
*/

$simpletracker_version = "0.33";

class SimpleTracker
{

	function it_menus() {
		add_options_page('WP Simple Tracker Settings', 'Simple Tracker', 'manage_options', 'wp-simpletracker', array(&$this, 'it_main_settings'));
	}

	function it_main_settings() {
		global $wpdb;
		
		//must check that the user has the required capability 
		if (!current_user_can('manage_options')) {
			wp_die( __('You do not have sufficient permissions to access this page.') );
		}

		wp_enqueue_script('simpletracker_dnd');
		wp_enqueue_script('simpletracker_adminjs');
		wp_enqueue_style('simpletracker_admincss');

		if( isset($_POST['submit']) && $_POST['submit'] == '1' ) {
			$i = 1;
			$wpdb->query("DELETE FROM {$wpdb->prefix}it_type");
			if (isset($_POST['type_names']))
			foreach ($_POST['type_names'] as $k => $v) {
				$wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}it_type
											(type_id, type_name, type_colour, type_tracker, type_order) 
											VALUES (%d, %s, %s, 1, %d)",
											$k,
											$_POST['type_names'][$k],
											$_POST['type_colours'][$k],
											$i++));
			}
			
			$i = 1;
			$wpdb->query("DELETE FROM {$wpdb->prefix}it_status");
			if (isset($_POST['status_names']))
			foreach ($_POST['status_names'] as $k => $v) {
				$wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}it_status
											(status_id, status_name, status_colour, status_strike, status_tracker, status_order)
											VALUES (%d, %s, %s, %d, 1, %d)",
											$k,
											$_POST['status_names'][$k],
											$_POST['status_colours'][$k],
											isset($_POST['status_strikes'][$k]) ? 1 : 0,
											$i++));
			}
			
		?>
		<div class="updated"><p><strong><?php _e('settings saved.', 'menu-test' ); ?></strong></p></div>
		<?php
		}
			echo '<div class="wrap">';
			echo "<h2>" . __( 'WP-SimpleTracker Settings', 'menu-test' ) . "</h2>";
			?>
		<form name="" method="post" action="">
			<input type="hidden" name="submit" value="1">
			<h3>Types</h3>
			<a href="" onclick="return append_new_type();">Add New</a>
			<table class="form-table" id="types_table"> 
				<?php
				$types = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}it_type WHERE type_tracker = 1 ORDER BY type_order ASC, type_id ASC");
				foreach ($types as $type) {
					?>
					<tr valign="top"> 
						<td class="dragger"></td><td><input name="type_names[<?php echo $type->type_id?>]" type="text" id="" value="<?php echo stripslashes($type->type_name) ?>" /><input name="type_colours[<?php echo $type->type_id?>]" type="text" id="" value="<?php echo $type->type_colour ?>" /> <a onclick="jQuery(this).parent().parent().remove(); return false;" href="">delete</a></td> 
					</tr>	
					<?php
				}
				?>
			</table>
			<script type="text/javascript" charset="utf-8">
				function append_new_type() {
					new_row = "<tr valign='top'><td class='dragger'></td><td><input name='type_names[]' type='text' /><input name='type_colours[]' type='text' /><a onclick='jQuery(this).parent().parent().remove(); return false;' href=''>delete</a></td></tr>";
					jQuery('#types_table').append(new_row);
					jQuery("#types_table").tableDnD()
					// jQuery("#status_table").tableDnD()
					return false;
				}
			</script>
			
			
			
			<h3>Status</h3>
			<a href="" onclick="return append_new_status();">Add New</a>
			<table class="form-table" id="status_table"> 
				<?php
				$types = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}it_status WHERE status_tracker = 1 ORDER BY status_order ASC, status_id ASC");
				foreach ($types as $status) {
					?>
					<tr valign="top"> 
						<td class="dragger"></td>
						<td><input name="status_names[<?php echo $status->status_id?>]" type="text" id="" value="<?php echo stripslashes($status->status_name) ?>" /><input name="status_colours[<?php echo $status->status_id?>]" type="text" id="" value="<?php echo $status->status_colour ?>" /> <input type="checkbox" name="status_strikes[<?php echo $status->status_id?>]" <?php echo $status->status_strike ? 'checked' : ''?>> <a onclick="jQuery(this).parent().parent().remove(); return false;" href="">delete</a></td> 
					</tr>	
					<?php
				}
				?>
			</table>
			<script type="text/javascript" charset="utf-8">
				function append_new_status() {
					new_row = '<tr valign="top"><td class="dragger"></td><td><input name="status_names[]" type="text" /><input name="status_colours[]" type="text" /> <input type="checkbox" name="status_strikes[]"> <a onclick="jQuery(this).parent().parent().remove(); return false;" href="">delete</a></td></tr>';
					jQuery('#status_table').append(new_row);
					// jQuery("#types_table").tableDnD()
					jQuery("#status_table").tableDnD()
					return false;
				}
			</script>

			<p class="submit">
			<input type="submit" name="Submit" class="button-primary" value="<?php esc_attr_e('Save Changes') ?>" />
			</p>

		</form>
		</div>

		<?php
	}

	function user_has_permission($issue_id = 0) {
		if (current_user_can('simpletracker_manageissues')) {
			// good enough!
			return true;
		}
		// has to be the assignee to change
		$issue = $this->it_get_issue($issue_id);
		$user = wp_get_current_user();
		return $issue && $issue->issue_assignee == $user->ID;
	}

	function it_redirect($url) {
		// echo '<script type="text/javascript" charset="utf-8">
		// 	location.href = "'.$url.'";
		// </script>';
		// die;
		wp_redirect($url);
	}

	function it_action_handlers() {
		global $wpdb;
		$user = wp_get_current_user();
		$do = $this->get_request('do');
		$qdo = $this->get_request('qdo');	
		if ($do && $do == 'it_qdo' && $qdo) {
			if (!$user->ID) {
				wp_die('You do not have permission to be here. Please login first then try again.');
			}
			switch ($qdo) {
				case 'toggle_star':
					$id = $this->get_request('issue_id');
					$user = wp_get_current_user();
					$starred = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}it_starred
															WHERE user_id = %d AND issue_id = %d", $user->ID, $id));
					if ($starred) {
						$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}it_starred
													WHERE user_id = %d AND issue_id = %d", $user->ID, $id));
					} else {
						$wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}it_starred
								(user_id, issue_id) VALUES (%d, %d)", $user->ID, $id));
					}
					die;
					break;
				case 'delete_issue':
					if (!current_user_can('simpletracker_manageissues')) {
						wp_die("You don't have permissions to do that.");
					}
					$id = (int) $this->get_request('issue_id');
					$post_id = (int) $this->get_request('post_id');
					$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}it_comment WHERE comment_issue = %d", $id));
					$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}it_starred WHERE issue_id = %d", $id));
					$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}it_issue WHERE issue_id = %d", $id));
					$this->it_redirect($this->build_url('', $post_id));
					break;
				case 'delete_comment':
					if (!current_user_can('simpletracker_manageissues')) {
						wp_die("You don't have permissions to do that.");
					}
					$post_id = (int) $this->get_request('post_id');
					$id = $wpdb->get_var($wpdb->prepare("SELECT comment_issue FROM {$wpdb->prefix}it_comment
													WHERE comment_id = %d", $this->get_request('comment_id')));
					$wpdb->query($wpdb->prepare("DELETE FROM {$wpdb->prefix}it_comment WHERE comment_id = %d", $this->get_request('comment_id')));
					$this->it_redirect($this->build_url('do=view_issue&issue='.$id, $post_id));
					break;
				case 'save_issue':
					// will take all other params from _REQUEST
					$post_content = $wpdb->get_var($wpdb->prepare("SELECT post_content FROM $wpdb->posts
																WHERE ID = %d", $this->get_request('post_id')));
					preg_match('/<!--wp-simpletracker-(\d+)-->/', $post_content, $matches);
					$content = $this->it_save_changes($matches[1]);
					break;
				
			}
			die;
		}
	}

	// Action handler for 'the_content' - scans the content for our shortcode
	// and inserts the appropriate output if it's present - otherwise does
	// nothing.
	function it_output($content) {
		global $wpdb;
		global $post;

		preg_match('/<!--wp-simpletracker-(\d+)-->/', $content, $matches);

		if (!count($matches)) 
			return $content;
		// from the post content <!--wp-simpletracker-1--> means id: 1
		// it should only match ints anyways, but casting to be super safe
		$tracker_id = (int) $matches[1];

		// Now we know we're rendering our stuff, we can set up our javascript
		// variables and make sure it gets included...
		$scriptData = array(
			'plugin_url' => plugin_dir_url(__FILE__),
			'post_url' => $this->build_url());
		wp_localize_script('simpletracker_js', 'st', $scriptData);
		wp_enqueue_script('simpletracker_js');
		wp_enqueue_style('simpletracker_css');


		$user = wp_get_current_user();
		
		$do = $this->get_request('do');
		switch ($do) {
			case 'new_issue':
				// Report a new issue...
                $content .= $user->ID ? $this->it_changes_form($tracker_id) : $this->it_guest_login_text('main_list');
				break;
			case 'view_issue':
				// Viewing an individual issue...
				$issue_id = (int) $this->get_request('issue');
				$content = $this->it_view_issue($issue_id);
				break;
			case 'view':
				// Viewing a list of issues...
				$all = (int)$this->get_request('all');
				$content = $this->it_main_list($tracker_id, $all);
                break;
			default:
				// The default is basically the same as 'view' but defaulting to open only...
				$content = $this->it_main_list($tracker_id, 0);
                break;
		}
		return stripslashes($content);
	}

	// Get all issues matching the given criteria.
	//  $tracker_id is the tracker ID (always 1 currently)
	//  $all is 1 for all issues or 0 for just open ones
	function it_get_issues($tracker_id, $all) {
		global $wpdb;
		// in all cases should only send an int to here, but just to make sure
		$tracker_id = (int) $tracker_id;
		$all = (int) $all;

		$sql = "SELECT i.*, t.*, s.*, u.display_name as assignee_name, u2.display_name as reporter_name
								FROM {$wpdb->prefix}it_issue i
								JOIN {$wpdb->prefix}it_type t ON i.issue_type = t.type_id
								JOIN {$wpdb->prefix}it_status s ON i.issue_status = s.status_id
								LEFT JOIN {$wpdb->prefix}users u ON i.issue_assignee = u.ID
								JOIN {$wpdb->prefix}users u2 ON i.issue_reporter = u2.ID
								WHERE i.issue_tracker = $tracker_id ";
		if($all == 0) {
			// TODO: Fix status_strike, why can it be null?
			$sql .= 'AND (s.status_strike <> 1 OR ISNULL(status_strike))';
		}
		$sql .= 'ORDER BY i.issue_id ASC';
		$res = $wpdb->get_results($sql);
		if($res)
			return $res;
		return array();
	}

	function it_get_issue($id) {
		global $wpdb;
		$id = (int) $id;
		return $wpdb->get_row("SELECT i.*, t.*, s.*, u.display_name as assignee_name, u2.display_name as reporter_name
							FROM {$wpdb->prefix}it_issue i
							JOIN {$wpdb->prefix}it_type t ON i.issue_type = t.type_id
							JOIN  {$wpdb->prefix}it_status s ON i.issue_status = s.status_id
							LEFT JOIN {$wpdb->prefix}users u ON i.issue_assignee = u.ID
							JOIN {$wpdb->prefix}users u2 ON i.issue_reporter = u2.ID
							WHERE i.issue_id = '$id'");
	}

	// Check if the given user has the given issue starred.
	function is_starred($userid, $id) {
		global $wpdb;
		return $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}it_starred
			WHERE user_id = %d AND issue_id = %d", $userid, $id));
	}

	function star_issue($userid, $id) {
		global $wpdb;
		$wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}it_starred
			(user_id, issue_id) VALUES (%d, %d)", $userid, $id));
	}

	function it_view_issue($id) {
		global $post, $wpdb;

		$issue = $this->it_get_issue($id);
		if (!$issue) {
			return "Couldn't find that issue.";
		}
		$user = wp_get_current_user();
		$strike = $issue->status_strike ? 'strike' : '';
		$issue->issue_time = date('F jS, Y @ h:i a', $issue->issue_time);
		$plugin_url = plugin_dir_url(__FILE__);
		$starred = $this->is_starred($user->ID, $id);
		$header = $this->get_header();

		$delete_link = '';
		if (current_user_can('simpletracker_manageissues')) {
			$delete_link = '
			<div>
				<label><a href="'.$this->build_url('do=it_qdo&qdo=delete_issue&post_id='.$post->ID.'&issue_id=' . $id).'">Delete</a></label>
			</div>';
		}
		$onoff = $starred ? 'on' : 'off';
		$issue->issue_summary = ($issue->issue_summary);
		$issue->issue_description_bred = nl2br($issue->issue_description);
		$star = !$user->ID ? '' : <<<STAR
		<a title="Click to toggle star" href="javascript:toggle_star({$issue->issue_id})"><img id="star_{$issue->issue_id}" src="{$plugin_url}star_{$onoff}.gif" /></a>
STAR;
                $content = <<<HTML
                $header
		<div class="wp-simpletracker issues" id="response-div">
			<div>
				<label>ID#</label>
				<span>{$issue->issue_id} $star</span>
			</div>
			<div>
				<label>Summary</label>
				<span>{$issue->issue_summary}</span>
			</div>
			<div>
				<label>Type</label>
				<span style="background:#{$issue->type_colour}">{$issue->type_name}</span>
			</div>
			<div>
				<label>Status</label>
				<span style="background:#{$issue->status_colour};" class="$strike">{$issue->status_name}</span>
			</div>
			
			<div>
				<label>Reported on</label>
				<span>{$issue->issue_time}</span>
			</div>
			<div>
				<label>Reporter</label>
				<span>{$issue->reporter_name}</span>
			</div>

			<div>
				<label>Assignee</label>
				<span>{$issue->assignee_name}</span>
			</div>

			<div>
				<label>Description</label>
				<span>{$issue->issue_description_bred}</span>
			</div>
			$delete_link
		</div>
HTML;
		// again, just making sure we're safe
		$id = (int) $id;
		$comments = $wpdb->get_results("SELECT c.*, u.display_name, u.user_email
										FROM {$wpdb->prefix}it_comment c
										JOIN {$wpdb->prefix}users u ON c.comment_poster = u.ID
										JOIN {$wpdb->prefix}it_issue i ON c.comment_issue = i.issue_id
										WHERE i.issue_id = '$id'
										ORDER BY c.comment_time ASC");
		$content .= '<div id="comments"><h3 id="comments-title">'.number_format(count($comments)).' comments</h3> <ol class="commentlist">';
		foreach ($comments as $comment) {
			$comment->comment_body = ($comment->comment_body);
			$comment->comment_time = date('F jS, Y @ h:i a', $comment->comment_time);
			$avatar = $this->get_gravatar($comment->user_email, 40, 404, 'g', true, array('class' => 'avatar avatar-40 photo'));
			$delete_url = $this->build_url('do=it_qdo&qdo=delete_comment&post_id='.$post->ID.'&comment_id='.$comment->comment_id);
			$delete_link = current_user_can('simpletracker_manageissues') ? " <a class='comment-edit-link' href='$delete_url'>(Delete)</a>" : '';
			$comment->comment_body = nl2br($comment->comment_body);
			$content .= <<<HTML
			<li class="comment"> 
				<div> 
					<div class="comment-author"> 
						{$avatar}
						<cite>{$comment->display_name}</cite>	
					</div>
					<div class="comment-meta"><a href="javascript:void(0);">{$comment->comment_time}</a>$delete_link</div>
					<div class="comment-body">{$comment->comment_body}</div>
				</div>
			</li>
HTML;
		}
		$content .= '</ol></div><hr />';
		if ($user->ID) $content .= $this->it_changes_form($issue->issue_tracker, $issue);
		else $content .= $this->it_guest_login_text('issue_page');
		return $content;
	}

	function it_guest_login_text($from = 'issue_page') {
		switch ($from) {
			case 'issue_page':
				$motive = 'comment';
				break;
			
			case 'main_list':
				$motive = 'submit a new issue';
				break;
		}
		return "<p><a href='".wp_login_url()."'>Login</a> to $motive</p>";
	}

	function get_request($index) {
		return isset($_REQUEST[$index]) ? $_REQUEST[$index] : '';
	}

	function it_save_changes($tracker_id) {
		global $wpdb, $post;
		$user = wp_get_current_user();
		$changes = array();
		$issue_id = (int) $this->get_request('issue_id');
		$post_id = (int) $this->get_request('post_id');
		$and_more = '';
		if ($issue_id) {
			// we're either commenting or making a change, figure out the changes first
			$issue = $this->it_get_issue($issue_id);
			$and_more = 'do=view_issue&issue=' . $issue_id; 
			
			if ($this->user_has_permission($issue_id)):
				// lets compare the changes?
				$new = new stdClass;
				$new->issue_summary = wp_kses_data($this->get_request('summary'));
				$new->issue_type = $this->get_request('type');
				$new->issue_status = $this->get_request('status');
				$new->issue_description = wp_kses_data($this->get_request('description'));
				$new->issue_assignee = $this->get_request('assignee');
			
				if ($new->issue_summary != $issue->issue_summary) {
					$changes[] = "<strong>Summary</strong> {$new->issue_summary}";
				}
				if ($new->issue_type != $issue->issue_type) {
					$old_type = $wpdb->get_row("SELECT type_name, type_colour FROM {$wpdb->prefix}it_type WHERE type_id={$issue->issue_type}");
					$id = (int) $new->issue_type;
					$new_type = $wpdb->get_row("SELECT type_name, type_colour FROM {$wpdb->prefix}it_type WHERE type_id=$id");
					$changes[] = "<strong>Type</strong> <span style='background:#{$old_type->type_colour}'>{$old_type->type_name}</span> > <span style='background:#{$new_type->type_colour}'>{$new_type->type_name}</span>";
				}
				if ($new->issue_status != $issue->issue_status) {
					$old_status = $wpdb->get_row("SELECT status_name, status_colour, status_strike FROM {$wpdb->prefix}it_status WHERE status_id={$issue->issue_status}");
					$strike1 = $old_status->status_strike ? 'strike' : '';
					$id = (int) $new->issue_status;
					$new_status = $wpdb->get_row("SELECT status_name, status_colour, status_strike FROM {$wpdb->prefix}it_status WHERE status_id=$id");
					$strike2 = $new_status->status_strike ? 'strike' : '';
					$changes[] = "<strong>Status</strong> <span style='background:#{$old_status->status_colour}' class='$strike1'>{$old_status->status_name}</span> > <span style='background:#{$new_status->status_colour}' class='$strike2'>{$new_status->status_name}</span>";
				}
				if ($new->issue_description != $issue->issue_description) {
					$changes[] = "<strong>Description</strong> {$new->issue_description}";
				}
				if ($new->issue_assignee != $issue->issue_assignee) {
					$id = (int) $new->issue_assignee;
					$new_assignee_name = '--';
					if ($id) {
						$new_assignee = $wpdb->get_row("SELECT display_name FROM $wpdb->users WHERE ID=$id");
						$new_assignee_name = $new_assignee->display_name;
						if(!$this->is_starred($id, $issue->issueid))
							$this->star_issue($id, $issue->issueid);
					}
					$changes[] = "<strong>Assignee</strong> {$new_assignee_name}";
				}
				// write the new issue details
				$wpdb->query($wpdb->prepare("UPDATE {$wpdb->prefix}it_issue
											SET issue_summary = %s,
											issue_type = %d,
											issue_status = %d,
											issue_description = %s,
											issue_assignee = %d
											WHERE issue_id = %d",
											$new->issue_summary,
											$new->issue_type,
											$new->issue_status,
											$new->issue_description,
											$new->issue_assignee,
											$issue_id));
			endif;
			
			$changes_stuff = count($changes) ? '<p>' . implode('<br />', $changes) . '</p>' : '';
			$comment = $changes_stuff . '<p>' . wp_kses_data($this->get_request('comment')) . '</p>';

			//, and make the comment
			if (strlen($comment) > 7) {
				// first make sure this commment has not been submitted recently
				$minutes = 10;
				$already_made = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}it_comment
															WHERE comment_issue = %d AND comment_poster = %d 
																			AND comment_body = %s AND comment_time > %d",
															$issue_id, $user->ID, $comment, time() - $minutes * 60));
				if (!$already_made) {				
					$wpdb->query($wpdb->prepare("INSERT INTO {$wpdb->prefix}it_comment
										(comment_issue, comment_poster, comment_body, comment_time)
										VALUES (%d, %d, %s, %d)", $issue_id, $user->ID, $comment, time()));
					$this->announce_change_to_stars($comment, $issue_id);

					// Auto-star when making a comment (after announcing, so you don't
					// get notified of your first change.
					if(!$this->is_starred($user->ID, $issue_id))
						$this->star_issue($user->ID, $issue_id);

				}
			}
		} else {
			// we're submitting a new issue, save to db
			$can = $this->user_has_permission(0);
			$default_status = $wpdb->get_var("SELECT status_id FROM {$wpdb->prefix}it_status ORDER BY status_order ASC, status_id ASC LIMIT 0, 1");
			$wpdb->query($wpdb->prepare("
				INSERT INTO {$wpdb->prefix}it_issue
				(issue_tracker, issue_summary, issue_type, issue_status, issue_time, issue_reporter, issue_description, issue_assignee)
				VALUES (%d, %s, %d, %d, %d, %d, %s, %d)", 
						$tracker_id,
						wp_kses_data($this->get_request('summary')),
						$this->get_request('type'),
						$can ? $this->get_request('status') : $default_status,
						time(),
						$user->ID,
						wp_kses_data($this->get_request('description')),
						$can ? $this->get_request('assignee') : 0));
			$newid = $wpdb->insert_id;
			$this->star_issue($user->ID, $newid);
			$and_more = 'do=view_issue&issue=' . $newid; 
		}
		$this->it_redirect($this->build_url($and_more, $post_id));
	}

	// Get the form for making a change to an existing issue, or for creating a
	// new one if $issue is null.
	function it_changes_form($tracker_id, $issue = null) {
		global $wpdb, $post;
		$user = wp_get_current_user();
		if ($issue) {
			$form_h3 = 'Change';
			$form_submit = 'Save changes';
		} else {
			$issue = new stdClass;
			$issue->issue_id = 0;
			$issue->issue_summary = '';
			$issue->issue_description = '';
			$issue->type_id = 0;
			$issue->status_id = 0;
			$issue->issue_assignee = 0;
			$form_h3 = 'Submit new issue';
			$form_submit = 'Submit';
		}
		
		$types = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}it_type 
									WHERE type_tracker = $tracker_id
									ORDER BY type_order ASC, type_id ASC");
		$types_options = '';
		foreach ($types as $type) {
			$sel = $type->type_id == $issue->type_id ? ' selected' : '';
			$types_options .= "<option value={$type->type_id}{$sel}>{$type->type_name}</option>";
		}
		
		$comment_area = '';
		if ($issue->issue_id) {
		$comment_area = <<<HTML
			<p ><label for="comment">Comment</label><textarea id="comment" name="comment" cols="45" rows="8" ></textarea></p>	
HTML;
		}
		$issue_options = '';
		if ($this->user_has_permission($issue->issue_id)) {
		
			$statuses = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}it_status 
										WHERE status_tracker = $tracker_id
										ORDER BY status_order ASC, status_id ASC");
			$status_options = '';
			foreach ($statuses as $status) {
				$sel = $status->status_id == $issue->status_id ? ' selected' : '';
				$status_options .= "<option value={$status->status_id}{$sel}>{$status->status_name}</option>";
			}
		
			$users = $wpdb->get_results("SELECT u.ID, u.display_name FROM $wpdb->users u");
		
			$users_options = '<option value=0></option>';
			foreach ($users as $user) {
				$isassignee = $user->ID == $issue->issue_assignee;
				if($isassignee || user_can($user->ID, 'simpletracker_assignee')) {
					$sel = $isassignee ? ' selected' : '';
					$users_options .= "<option value={$user->ID}{$sel}>{$user->display_name}</option>";
				}
			}
			
			$issue_options = <<<OPTS
    <p ><label for="summary">Summary</label><input id="summary" name="summary" type="text" value="{$issue->issue_summary}" size="30" aria-required='true' /></p> 
    <p ><label for="description">Description</label><textarea id="description" name="description" cols="45" rows="8">{$issue->issue_description}</textarea></p>
    <p ><labelfor="type">Type</label><select name="type" id="type" >{$types_options}</select></p>
    <p ><label for="status">Status</label><select name="status" id="status" >{$status_options}</select></p>
    <p ><label for="assignee">Assignee</label><select name="assignee" id="assignee" >{$users_options}</select></p>
OPTS;
		} else if(!$issue->issue_id) {
			$issue_options = <<<OPTS
    <p ><label for="summary">Summary</label><input id="summary" name="summary" type="text" value="{$issue->issue_summary}" size="30" aria-required='true' /></p> 
    <p ><label for="description">Description</label><textarea id="description" name="description" cols="45" rows="8">{$issue->issue_description}</textarea></p>
    <p ><labelfor="type">Type</label><select name="type" id="type" >{$types_options}</select></p>
OPTS;
		}
		$action = $this->build_url('do=it_qdo&qdo=save_issue&post_id='.$post->ID);
		return <<<HTML
	        <div id="respond" style="border-top:0;">
	        <h3 id="reply-title">{$form_h3}</h3>
		<form action="$action" method="post" id="commentform"> 	
			<input type="hidden" name="issue_id" value="{$issue->issue_id}" id="issue_id">
			{$comment_area}
			{$issue_options}
			<p class="form-submit"> 
				<input name="submit" type="submit" id="submit" value="{$form_submit}" /> 
			</p> 
		</form>
		</div>
HTML;
	}

	function build_url($uri_string = '', $post_id = 0) {
		if (!$post_id) {
			global $post;	
			$post_id = $post->ID;
		}
		$permalink = get_permalink($post_id);
		return $permalink . (strpos($permalink, '?') === false ? '?' : '&') . $uri_string;
	}

        function get_header() {
		$urlopen = $this->build_url('');
		$urlall = $this->build_url('do=view&all=1');
		$urlnew = $this->build_url('do=new_issue');
        $header = <<<HTML
		<div style="text-align:center;">
		<p>List: <a href="{$urlopen}">open issues</a>, <a href="{$urlall}">all issues</a> Report: <a href="{$urlnew}">new issue</a></p>
		</div>
HTML;
                return $header;
        }

	// $all is 1 for all issues, 0 for open only
	function it_main_list($tracker_id, $all) {
		global $post, $wpdb;

		$all = (int)$all;

		$header = $this->get_header();

		$user = wp_get_current_user();
		$star = !$user->ID ? '' : <<<STAR
		<th style="width:1%;"> </th>
STAR;
		$content = <<<HTML
		<style type="text/css" media="screen">
			#content table.wp-simpletracker tr th, #content table.wp-simpletracker tr td {
				font-size: 12px;
				padding: 10px;
			}
		</style>

                $header
		<table id="wp-simpletracker-$tracker_id" class="wp-simpletracker">
			<tr>
				<th style="width:1%;">ID</th>
				<th style="width:95%;">Summary</th>
				<th style="width:1%;">Type</th>
				$star
			</tr>
HTML;
		
		$plugin_url = plugin_dir_url(__FILE__);
		$issues = $this->it_get_issues($tracker_id, $all);
		foreach ($issues as $issue) {
			$strike = $issue->status_strike ? 'strike' : '';
			$starred = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$wpdb->prefix}it_starred
				WHERE user_id = %d AND issue_id = %d", $user->ID, $issue->issue_id));
			$onoff = $starred ? 'on' : 'off';
			$url = $this->build_url('do=view_issue&issue='.$issue->issue_id);
			$issue->issue_summary = ($issue->issue_summary);
			$star = !$user->ID ? '' : <<<STAR
		<td><a title="Click to toggle star" href="javascript:toggle_star({$issue->issue_id})"><img id="star_{$issue->issue_id}" src="{$plugin_url}star_{$onoff}.gif" /></a></td>	
STAR;
			$content .= <<<HTML
			<tr style="background:#{$issue->status_colour};" >
				<td>{$issue->issue_id}</td>
				<td class="$strike"><a href="$url">{$issue->issue_summary}</a></td>
				<td style="background:#{$issue->type_colour};">{$issue->type_name}</td>
				$star
			</tr>
HTML;
		}
		$content .= "</table>";
		
		$statuses = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}it_status WHERE status_tracker = $tracker_id ORDER BY status_order ASC, status_id ASC");
		$arr = array();
		foreach ($statuses as $s) {
			$strike = $s->status_strike ? 'strike' : '';
			$arr[] = "<span style='background:#{$s->status_colour};' class='$strike'>{$s->status_name}</span>";
		}
		
		$content .= '<p>Status: ' . implode(', ', $arr) . '</p><hr />';
		
		return $content;
	}

	function announce_change_to_stars($comment, $issue_id) {
		global $wpdb, $post;
		$issue_id = (int) $issue_id;
		$poster = wp_get_current_user();
		$users = $wpdb->get_results($wpdb->prepare("SELECT DISTINCT u.user_email, u.ID FROM $wpdb->users u
											JOIN {$wpdb->prefix}it_starred s ON s.user_id = u.ID
											WHERE s.issue_id = %d AND s.user_id <> %d", $issue_id, $poster->ID));
		$headers= "MIME-Version: 1.0\n" .
                    "Content-Type: text/html; charset=\"" . get_option('blog_charset') . "\"\n";
                $link = $this->build_url('do=view_issue&issue='.$issue_id, (int) $this->get_request('post_id'));
		foreach ($users as $user) {
			wp_mail($user->user_email, 
				'[WP-IT] Changes to issue #'.$issue_id, 
				'<p>New comment by '.$poster->display_name.'</p>'.$comment . '<p>Click here to go to the issue: <a href="'.$link.'">'.$link.'</a></p><p>-- Powered by WP-SimpleTracker</p>',
				$headers);
		}
	}

	function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
		$url = 'http://www.gravatar.com/avatar/';
		$url .= md5( strtolower( trim( $email ) ) );
		$url .= "?s=$s&d=$d&r=$r";
		if ( $img ) {
			$url = '<img src="' . $url . '"';
			foreach ( $atts as $key => $val )
				$url .= ' ' . $key . '="' . $val . '"';
			$url .= ' />';
		}
		return $url;
	}

	// Installer - registered as an activation hook (and called independently 
	// after an upgrade) to create/update database tables.
	function install() {
		global $wpdb;

		$main_table = $wpdb->prefix . "it_tracker";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		$sql = "CREATE TABLE " . $main_table . " (
					id int(3) NOT NULL AUTO_INCREMENT,
					UNIQUE KEY id (id)
				);";
		dbDelta($sql);
		
		$sql = "CREATE TABLE {$wpdb->prefix}it_issue (
					issue_id int(11) NOT NULL AUTO_INCREMENT,
					issue_tracker int(3),
					issue_summary varchar(256),
					issue_type int(3),
					issue_status int(3),
					issue_description text,
					issue_assignee int(11) default 0,
					issue_reporter int(11),
					issue_time int(11),
					UNIQUE KEY issue_id (issue_id)
				);";
		dbDelta($sql);
		
		$sql = "CREATE TABLE {$wpdb->prefix}it_type (
					type_id int(3) NOT NULL AUTO_INCREMENT,
					type_tracker int(3),
					type_name varchar(64),
					type_colour varchar(6),
					type_order int(3),
					UNIQUE KEY type_id (type_id)
				);";
		dbDelta($sql);
		
		$sql = "CREATE TABLE {$wpdb->prefix}it_status (
					status_id int(3) NOT NULL AUTO_INCREMENT,
					status_tracker int(3),
					status_name varchar(64),
					status_colour varchar(6),
					status_strike int(1),
					status_order int(3),
					UNIQUE KEY status_id (status_id)
				);";
		dbDelta($sql);
		
		$sql = "CREATE TABLE {$wpdb->prefix}it_comment (
					comment_id int(11) NOT NULL AUTO_INCREMENT,
					comment_issue int(11),
					comment_poster int(11),
					comment_body text,
					comment_time int(11),
					UNIQUE KEY comment_id (comment_id)
				);";
		dbDelta($sql);
		
		$sql = "CREATE TABLE {$wpdb->prefix}it_starred (
					user_id int(11),
					issue_id int(11)
				);";
		dbDelta($sql);
		
		$installed = $wpdb->get_var("SELECT COUNT(*) FROM $main_table");
		if (!$installed) {
			/* default tracker */
			$wpdb->insert( $main_table, array('id' => 1) );

			/* types */
			$wpdb->insert("{$wpdb->prefix}it_type", array('type_name' => 'Bug', 'type_colour' => 'FFB4AD', 'type_tracker' => 1));
			$wpdb->insert("{$wpdb->prefix}it_type", array('type_name' => 'New Feature', 'type_colour' => 'E1FFDF', 'type_tracker' => 1));
			$wpdb->insert("{$wpdb->prefix}it_type", array('type_name' => 'Improvement', 'type_colour' => 'FFFFCF', 'type_tracker' => 1));
			$wpdb->insert("{$wpdb->prefix}it_type", array('type_name' => 'Feedback', 'type_colour' => 'E5D4E7', 'type_tracker' => 1));

			/* statuses */
			$wpdb->insert("{$wpdb->prefix}it_status", array('status_name' => 'Assigned', 
																					'status_tracker' => 1));
			$wpdb->insert("{$wpdb->prefix}it_status", array('status_name' => 'Resolved', 
								'status_strike' => 1, 'status_colour' => 'B8EFB3', 'status_tracker' => 1));
			$wpdb->insert("{$wpdb->prefix}it_status", array('status_name' => 'Won\'t Fix', 
								'status_strike' => 1, 'status_colour' => 'FFB4AD', 'status_tracker' => 1));
			$wpdb->insert("{$wpdb->prefix}it_status", array('status_name' => 'Duplicate', 
															'status_strike' => 1, 'status_tracker' => 1));
		}

                add_role(
                    'simpletracker_assignee',
                    'Issue Assignee',
                    array('read' => true,
                          'simpletracker_manageissues' => false,
                          'simpletracker_assignee' => true,
                    )
                );
                add_role(
                    'simpletracker_manager',
                    'Issue Manager',
                    array('read' => true,
                          'simpletracker_manageissues' => true,
                          'simpletracker_assignee' => true,
                    )
                );

		global $simpletracker_version;
		update_option('simpletracker_version', $simpletracker_version);
	}

	// Handler for the wp_enqueue_scripts action. Register our javascript and css,
	// which we'll only enqueue later if we're actually using it.
	function enqueue_scripts() {
		$url = plugin_dir_url(__FILE__);
		wp_register_script('simpletracker_js',
			$url .  'javascripts.js', array('jquery'));
		wp_register_style('simpletracker_css', $url . 'style.css');
	}

	// Handler for the admin_enqueue_scripts action. Register our javascript and css,
	// which we'll only enqueue later if we're actually using it.
	function admin_enqueue_scripts() {
		$url = plugin_dir_url(__FILE__);
		wp_register_script('simpletracker_dnd',
			$url .  'jquery.tablednd.js', array('jquery'));
		wp_register_script('simpletracker_adminjs',
			$url .  'admin.js', array('jquery'));
		wp_register_style('simpletracker_admincss', $url . 'admin.css');
	}

	// Check if the plugin has been upgraded and update things if required.
	function upgrade_check() {
		global $simpletracker_version;
		if(get_site_option('simpletracker_version' ) != $simpletracker_version)
			$this->install();
	}


	// Constructor...
	function SimpleTracker() {
		# filter the content -- match the <!--wp-simpletracker--> and work on it
		add_filter('the_content', array(&$this, 'it_output'), 999);

		# add action hooks
		add_action('plugins_loaded', array(&$this, 'upgrade_check'));
		add_action('admin_menu', array(&$this, 'it_menus'));
		add_action('init', array(&$this, 'it_action_handlers'));
		add_action('wp_enqueue_scripts', array(&$this, 'enqueue_scripts'));
		add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_scripts'));

	}

}

register_activation_hook(__FILE__, array('SimpleTracker', 'install'));
$wp_simpletracker = new SimpleTracker();

